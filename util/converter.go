package util

import (
    "strconv"
)

func StringToUint64(value string) uint64 {
    ID, _ := strconv.Atoi(value)

    return uint64(ID)
}
