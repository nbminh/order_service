package model

import (
    "github.com/jinzhu/gorm"
)

type Order struct {
    gorm.Model
    OrderID             string
    Number              uint64
    OrderNumber         string
    OrderName           string
    Currency            string
    PresentmentCurrency string
    LocationID          string
    StatusOrder         string
    CustomerID          string
    FirstName           string
    LastName            string
    Email               string
    StatusCustomer      string
    ShippingPhone       string
    SubTotalDiscounts   string
    TotalDiscounts      string
    TotalPrice          string
    RefundedTotalPrice  string
    SubTotalPrice       string
    TotalTax            string
    OrderStatusUrl      string
}
