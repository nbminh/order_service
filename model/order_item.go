package model

import "github.com/jinzhu/gorm"

type OrderItem struct {
    gorm.Model
    ItemID       string
    OrderID      string
    ProductID    string
    CustomerID   string
    Name         string
    Price        string
    Quantity     string
    Sku          string
    TotalPrice   string
    TaxLinePrice string
    TaxLineRate  string
    TaxLineTitle string
}
