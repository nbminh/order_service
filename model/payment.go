package model

import (
    "github.com/jinzhu/gorm"
)

type Payment struct {
    gorm.Model
    OrderID     string
    PaymentID   string
    Code        string
    Name        string
    Refund      string
    Currency    string
    Amount      string
    Type        string
    Status      string
    ProcessedAt string
}
