package database

import (
    "gitlab.com/minh2101/order_service/model"
    "gorm.io/driver/mysql"
    "gorm.io/gorm"
)

var DB *gorm.DB

const (
    DB_USERNAME = "root"
    DB_PASSWORD = "my-secret-pw"
    DB_NAME     = "shopify"
    DB_HOST     = "localhost"
    DB_PORT     = "3307"
)

func ConnectDB() (*gorm.DB, error) {
    var err error
    dsn := DB_USERNAME + ":" + DB_PASSWORD + "@tcp" + "(" + DB_HOST + ":" + DB_PORT + ")/" + DB_NAME + "?" + "parseTime=true&loc=Local"

    orm, err := gorm.Open(mysql.Open(dsn))

    if nil != err {
        return nil, err
    }

    orm.AutoMigrate(&model.Order{}, &model.OrderItem{}, &model.Payment{})
    DB = orm

    return DB, err
}
