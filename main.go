package main

import (
    "context"
    "github.com/grpc-ecosystem/grpc-gateway/runtime"
    "github.com/spf13/viper"
    "gitlab.com/minh2101/order_service/order"
    "gitlab.com/minh2101/order_service/util"
    pb "gitlab.com/minh2101/shopify_proto/order"
    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials/insecure"
    "log"
    "net"
    "net/http"
)

func main() {
    hermes := viper.GetString("hermes")
    //c := make(chan os.Signal, 1)
    platform := &util.Platform{
        Hermes: hermes,
    }
    //Create a gRPC server object
    grpcServer := grpc.NewServer()

    ordersService := order.NewOrderService(platform)
    pb.RegisterOrderServiceServer(grpcServer, ordersService)

    //Create a listener on TCP port
    lis, err := net.Listen("tcp", ":8081")
    if err != nil {
        log.Fatalln("Failed to listen:", err)
    }
    // Serve gRPC server
    log.Println("Serving gRPC on 0.0.0.0:8081")
    go func() {
        log.Fatalln(grpcServer.Serve(lis))
    }()

    conn, err := grpc.DialContext(
        context.Background(),
        "0.0.0.0:8081",
        grpc.WithBlock(),
        grpc.WithTransportCredentials(insecure.NewCredentials()),
    )
    if err != nil {
        log.Fatalln("Failed to dial server:", err)
    }

    gwmux := runtime.NewServeMux()
    // Register User Service
    err = pb.RegisterOrderServiceHandler(context.Background(), gwmux, conn)
    if err != nil {
        log.Fatalln("Failed to register gateway:", err)
    }
    gwServer := &http.Server{
        Addr:    ":8091",
        Handler: gwmux,
    }
    log.Println("Serving gRPC-Gateway on 0.0.0.0:8091")
    log.Fatalln(gwServer.ListenAndServe())
}
