package order

import (
    "context"
    "fmt"

    "google.golang.org/grpc"
    "google.golang.org/protobuf/types/known/timestamppb"

    "gitlab.com/minh2101/order_service/database"
    "gitlab.com/minh2101/order_service/model"
    "gitlab.com/minh2101/order_service/util"
    pb "gitlab.com/minh2101/shopify_proto/order"
    ppb "gitlab.com/minh2101/shopify_proto/platform"
)

var DB, err = database.ConnectDB()

type OrderService struct {
    platform *util.Platform
    pb.UnimplementedOrderServiceServer
}

func NewOrderService(platform *util.Platform) *OrderService {
    return &OrderService{
        platform: platform,
    }
}
func (s *OrderService) Get(ctx context.Context, r *pb.OrderRequest) (*pb.Order, error) {
    conn, err := grpc.Dial(":9001", grpc.WithInsecure())
    if nil != err {
        return nil, nil
    }
    defer conn.Close()
    platformService := ppb.NewOrderServiceClient(conn)
    resultOrder, err := platformService.Get(context.Background(), &ppb.OneOrderRequest{
        Id: r.GetId(),
    })
    if nil != err || nil == resultOrder {
        return nil, err
    }
    return resultOrder, nil
}

func (s *OrderService) Create(ctx context.Context, r *pb.CUOrderRequest) (*pb.Order, error) {
    conn, err := grpc.Dial(":9001", grpc.WithInsecure())
    if nil != err {
        return nil, err
    }
    defer conn.Close()
    platformService := ppb.NewOrderServiceClient(conn)

    entity, err := platformService.Create(context.Background(), &ppb.CUOrderRequest{
        Order: r.GetOrder(),
    })
    if nil != err || nil == entity {
        return nil, err
    }

    createOrder := PrepareDataToCreateOrder(entity)
    createOrderItem := PrepareDataToCreateOrderItem(entity, entity.Items)
    createOrderPayment := PrepareDataToCreatePayment(entity, entity.Payments)
    DB.Create(&createOrder)
    DB.Create(&createOrderItem)
    DB.Create(&createOrderPayment)

    return entity, nil
}

func (s *OrderService) Refund(ctx context.Context, r *pb.RefundRequest) (*pb.Order, error) {
    conn, err := grpc.Dial(":9001", grpc.WithInsecure())
    if nil != err {
        return nil, err
    }
    defer conn.Close()
    platformService := ppb.NewOrderServiceClient(conn)

    entity, err := platformService.Refund(ctx, &ppb.RefundRequest{
        Id:         r.GetOrderId(),
        Items:      r.GetItems(),
        Payments:   r.GetPayments(),
        LocationId: r.GetLocationId(),
        Note:       r.GetNote(),
    })

    return entity, err
}

func (s *OrderService) ListOrderDB(ctx context.Context, r *pb.Order) (*pb.ListOrder, error) {
    var order []*model.Order
    var orderItem []model.OrderItem
    var payments []model.Payment

    DB.Where("deleted_at is null").Order("created_at desc").Limit(100).Find(&order)

    var list []*pb.Order
    for _, u := range order {
        listUser := &pb.Order{
            Id:                 u.OrderID,
            Name:               u.OrderName,
            Number:             fmt.Sprintf("%d", u.Number),
            OrderNumber:        u.OrderNumber,
            Currency:           u.Currency,
            LocationId:         u.LocationID,
            Status:             u.StatusOrder,
            Customer:           DataCustomerOrderDBResponse(order),
            Items:              queryOrderItemDB(orderItem, u.OrderID),
            Payments:           queryOrderPaymentDB(payments, u.OrderID),
            TotalPrice:         u.TotalPrice,
            RefundedTotalPrice: u.RefundedTotalPrice,
            SubtotalPrice:      u.SubTotalPrice,
            TotalTax:           u.TotalTax,
            OrderStatusUrl:     u.OrderStatusUrl,
            CreatedAt:          timestamppb.New(u.CreatedAt),
            UpdatedAt:          timestamppb.New(u.UpdatedAt),
        }
        list = append(list, listUser)
    }
    return &pb.ListOrder{
        ListOrder: list,
    }, nil
}
