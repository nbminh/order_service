package order

import (
    "fmt"
    "strconv"

    "gitlab.com/minh2101/order_service/model"
    "gitlab.com/minh2101/order_service/util"
    "gitlab.com/minh2101/shopify_proto/customer"
    "gitlab.com/minh2101/shopify_proto/tax"

    pb "gitlab.com/minh2101/shopify_proto/order"
    paypb "gitlab.com/minh2101/shopify_proto/payment"
)

func PrepareDataToCreateOrder(r *pb.Order) *model.Order {
    return &model.Order{
        OrderID:     r.GetId(),
        Number:      util.StringToUint64(r.GetNumber()),
        OrderNumber: r.GetOrderNumber(),
        OrderName:   r.GetName(),
        Currency:    r.GetCurrency(),
        LocationID:  r.GetLocationId(),
        StatusOrder: r.GetStatus(),

        CustomerID: r.GetCustomer().GetId(),
        FirstName:  r.GetCustomer().GetFirstName(),
        LastName:   r.GetCustomer().GetLastName(),
        Email:      r.GetCustomer().GetEmail(),

        ShippingPhone:       r.ShippingAddress.Phone,
        PresentmentCurrency: r.GetPresentmentCurrency(),
        SubTotalDiscounts:   r.GetSubTotalDiscounts(),
        TotalDiscounts:      r.GetTotalDiscounts(),
        TotalPrice:          r.GetTotalPrice(),
        RefundedTotalPrice:  r.GetRefundedTotalPrice(),
        SubTotalPrice:       r.GetSubtotalPrice(),
        TotalTax:            r.GetTotalTax(),
        OrderStatusUrl:      r.GetOrderStatusUrl(),
    }
}

func PrepareDataToCreateOrderItem(order *pb.Order, item []*pb.OrderItem) []model.OrderItem {
    var orderItem []model.OrderItem
    for _, v := range item {
        ordItem := model.OrderItem{
            ItemID:       v.Id,
            OrderID:      order.Id,
            ProductID:    v.ProductId,
            CustomerID:   order.Customer.Id,
            Name:         v.Name,
            Price:        v.Price,
            Quantity:     fmt.Sprintf("%v", v.Quantity),
            Sku:          v.Sku,
            TotalPrice:   order.TotalPrice,
            TaxLinePrice: v.TaxLines[0].Price,
            TaxLineRate:  v.TaxLines[0].Rate,
            TaxLineTitle: v.TaxLines[0].Title,
        }
        orderItem = append(orderItem, ordItem)
    }
    return orderItem
}
func PrepareDataToCreatePayment(order *pb.Order, payInfo []*paypb.PaymentInfo) []model.Payment {
    var payment []model.Payment
    for _, v := range payInfo {
        ordPay := model.Payment{
            PaymentID:   fmt.Sprintf("%v", v.Id),
            OrderID:     order.Id,
            Code:        v.PaymentCode,
            Name:        v.PaymentName,
            Currency:    v.Currency,
            Amount:      v.Amount,
            Type:        v.Type,
            Status:      v.Status,
            ProcessedAt: v.ProcessedAt,
        }
        payment = append(payment, ordPay)
    }
    return payment
}
func DataCustomerOrderDBResponse(r []*model.Order) *customer.Customer {
    data := &customer.Customer{}
    for _, v := range r {
        data = &customer.Customer{
            Id:        v.CustomerID,
            FirstName: v.FirstName,
            LastName:  v.LastName,
            Email:     v.Email,
            Status:    v.StatusCustomer,
        }
    }
    return data
}
func DataTaxLineDBResponse(r []model.OrderItem) []*tax.TaxLine {
    if nil == r || 0 == len(r) {
        return nil
    }
    taxLine := make([]*tax.TaxLine, len(r))

    for i, v := range r {
        tax := &tax.TaxLine{
            Price: v.TaxLinePrice,
            Rate:  v.TaxLineRate,
            Title: v.TaxLineTitle,
        }

        taxLine[i] = tax
    }
    return taxLine

}

func queryOrderItemDB(orderItem []model.OrderItem, ID string) []*pb.OrderItem {
    id, _ := strconv.Atoi(ID)
    data := DB.Where("order_id = ?", id).First(&orderItem).RowsAffected
    if data == 0 {
        return nil
    }
    var OrderItem []*pb.OrderItem
    for _, v := range orderItem {
        quantity, _ := strconv.ParseFloat(v.Quantity, 32)
        quantity32 := float32(quantity)
        ordItem := &pb.OrderItem{
            Id:         v.ItemID,
            ProductId:  fmt.Sprintf("%v", v.ProductID),
            Price:      fmt.Sprintf("%v", v.Price),
            Name:       v.Name,
            Sku:        v.Sku,
            Quantity:   quantity32,
            TotalPrice: v.TotalPrice,
            TaxLines:   DataTaxLineDBResponse(orderItem),
        }
        OrderItem = append(OrderItem, ordItem)
    }
    return OrderItem
}

func queryOrderPaymentDB(payment []model.Payment, ID string) []*paypb.PaymentInfo {
    id, _ := strconv.Atoi(ID)
    data := DB.Where("order_id = ?", id).First(&payment).RowsAffected
    if data == 0 {
        return nil
    }
    var paymentInfo []*paypb.PaymentInfo
    for _, v := range payment {
        u, _ := strconv.ParseUint(v.PaymentID, 0, 64)
        payInfo := &paypb.PaymentInfo{
            Id:          u,
            PaymentCode: v.Code,
            PaymentName: v.Name,
            Currency:    v.Currency,
            Amount:      v.Amount,
            Type:        v.Type,
            Status:      v.Status,
            ProcessedAt: v.ProcessedAt,
        }
        paymentInfo = append(paymentInfo, payInfo)
    }
    return paymentInfo
}
